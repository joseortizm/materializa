Materializa
===========

Versión de [Materialize](https://materializecss.com/) adaptado para los estudiantes de [Capacitadero](https://gitlab.com/users/capacitadero/projects).

Contribución
------------

Envia un comentario, consulta y preguntas a: [José Ortiz](http://www.joseortizm.com/) u511656@upc.edu.pe

Versión accesible
------------

Revisa la versión accesible en [gitforall.com](http://gitforall.com/projects/capacitadero/materializa/index.html)

Plantilla básica de Materializa
-------------------------------

```html
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Materializa</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="css/materializa.min.css"  media="screen,projection"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>	    
</head>
<body>	
	<h1>Hola Materializa!</h1>
	<script type="text/javascript" src="js/materializa.min.js"></script>
</body>
</html>
```
